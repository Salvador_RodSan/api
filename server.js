//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req, res){
 res.sendFile(path.join(__dirname, 'index.html'));
});

app.get("/clientes/:idcliente",function(req, res){
 res.send('Aquì tiene al cliente nùemro :' + req.params.idcliente);
});

app.post("/", function(rec, res) {
  res.send('Hemos recibido su peticiòn de post cambiada');
});

app.put("/", function(rec, res) {
  res.send('Hemos recibido su peticiòn de put');
});

app.delete("/", function(rec, res) {
  res.send('Hemos recibido su peticiòn de delete');
});
